var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');
let words = require("./routes/words");

var app = express();

// view engine setup
// To set functioning of mustache view engine
var fs = require("fs");
var hogan = require("hogan.js");
app.engine('mustache', function (filePath, options, callback) {
    let layout = fs.readFileSync(path.join(__dirname, 'views/layout.mustache'));
    layout = hogan.compile(layout.toString());
    fs.readFile(filePath, function (err, content) {
        if (err) {
            return callback(err)
        }
        let template = hogan.compile(content.toString());
        let result = template.render(options);
        return callback(null, layout.render({}, {body: result}));
    });
});
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mustache');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/words', words);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
