let express = require('express');
let router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send(
        [
            {
                word: "hello",
                translations: ["привет", "здравствуй"],
                contexts: ["hello my friend"]
            },
            {
                word: "mind",
                translations: ["ум", "против"],
                contexts: ["don't you mind if I"]
            }
        ]
    );
});

module.exports = router;
