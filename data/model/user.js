let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let UserSchema = new Schema({
    id: Schema.Types.ObjectId,
    name: {
        type: String,
        required: [true, "field is required"]
    },
    username: {
        type: String,
        required: [true, "field is required"]
    },
    password: {
        type: String,
        required: [true, "field is required"]
    },
    email: {
        type: String,
        required: [true, "field is required"]
    },
    dictionaries: [{type: Schema.Types.ObjectId, ref: 'Dictionary'}]
});

module.exports = mongoose.model("User", UserSchema);