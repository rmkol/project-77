let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let DictionarySchema = new Schema({
    id: Schema.Types.ObjectId,
    language: {
        type: String,
        required: [true]
    },
    description: String,
    words: [{type: Schema.Types.ObjectId, ref: 'Word'}]
});

module.exports = mongoose.model("Dictionary", DictionarySchema);