let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let WordSchema = new Schema({
    id: Schema.Types.ObjectId,
    word: {
        type: String,
        required: [true]
    },
    language: {
        type: String,
        required: [true]
    },
    translations: [{type: String}],
    contexts: [{type: String}]
});

module.exports = mongoose.model("Word", WordSchema);